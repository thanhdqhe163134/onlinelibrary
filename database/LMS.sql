﻿CREATE DATABASE LMS;
go
USE LMS;
go

-- Tạo bảng Books
CREATE TABLE Books (
    book_id INT IDENTITY(1,1) PRIMARY KEY,
    title NVARCHAR(255),
    author NVARCHAR(255),
	publisher nvarchar(100),
    total_copies INT,
    copies_borrowed INT,
    copies_available INT,
	[description] NVARCHAR(MAX),
	IMG NVARCHAR(100),
	created_date DATETIME,
	created_user NVARCHAR(20),
	updated_date DATETIME,
	updated_user NVARCHAR(20),
	is_deleted BIT
);
CREATE TABLE BookDetail (
	detail_id INT IDENTITY(1,1) PRIMARY KEY,
    ISBN NVARCHAR(20),
	book_id INT,
    title NVARCHAR(255),
    author NVARCHAR(255),
	publisher nvarchar(100),
    publish_date DATE,
	created_date DATETIME,
	status NVARCHAR(200),
	is_borrowed BIT,
	updated_date DATETIME,
	updated_user NVARCHAR(20),
	is_deleted BIT
	FOREIGN KEY (book_id) REFERENCES Books (book_id)

);

-- Tạo bảng Users
CREATE TABLE Accounts (
    user_id INT IDENTITY(1,1) PRIMARY KEY,
    username NVARCHAR(50),
    password NVARCHAR(100),
    full_name NVARCHAR(100),
    email NVARCHAR(100),
    phone_number NVARCHAR(15),
    address NVARCHAR(255),
	role NVARCHAR(20),
	IMG NVARCHAR(100),
	created_date DATETIME,
	updated_date DATETIME,
	updated_user NVARCHAR(20),
	is_deleted BIT

);

-- Tạo bảng Loans
CREATE TABLE Loans (
    loan_id INT IDENTITY(1,1) PRIMARY KEY,
    user_id INT,
    detail_id INT,
    borrow_date DATETIME,
	due_date DATE,
    returned_date DATE,
	created_date DATETIME,
	created_user NVARCHAR(20),
	request_date DATETIME,
	expried_date DATETIME,
	is_returned BIT,
	is_approved BIT
    FOREIGN KEY (user_id) REFERENCES Accounts (user_id),
    FOREIGN KEY (detail_id) REFERENCES BookDetail (detail_id)
);

-- Tạo bảng Fine
CREATE TABLE Fine (
    fine_id INT IDENTITY(1,1) PRIMARY KEY,
    user_id INT,
    loan_id INT,
    fine_amount MONEY,
    [description] NVARCHAR(255),
    created_date DATE,
	created_user NVARCHAR(20),
	is_paid BIT,
    FOREIGN KEY (user_id) REFERENCES Accounts (user_id),
    FOREIGN KEY (loan_id) REFERENCES Loans (loan_id)
);


-- Tạo bảng Genres
CREATE TABLE Genres (
    genre_id INT IDENTITY(1,1) PRIMARY KEY,
    genre_name NVARCHAR(50),

);

CREATE TABLE Book_Genre (
    book_id INT,
    genre_id INT,
    PRIMARY KEY (book_id, genre_id),
    FOREIGN KEY (book_id) REFERENCES Books(book_id) ON DELETE CASCADE,
    FOREIGN KEY (genre_id) REFERENCES Genres(genre_id) ON DELETE CASCADE
);

-- Tạo bảng Request
CREATE TABLE Request (
	request_id INT IDENTITY(1,1) PRIMARY KEY,
    user_id INT,
	loan_id INT,
	request_date DATE,
    description NVARCHAR(255),
	created_date DATETIME,
	created_user NVARCHAR(20),
	is_approved BIT,
	FOREIGN KEY (loan_id) REFERENCES LOANS (loan_id),
	FOREIGN KEY (user_id) REFERENCES Accounts (user_id)
);

-- Tạo bảng Comment
CREATE TABLE Comment (
	comment_id INT IDENTITY(1,1) PRIMARY KEY,
    user_id INT,
	book_id INT,
	content NVARCHAR(MAX),
	rep_comment INT,
	created_date DATETIME,
	created_user NVARCHAR(20),
	updated_date DATETIME,
	updated_user NVARCHAR(20),
	is_deleted BIT,
	FOREIGN KEY (book_id) REFERENCES Books (book_id),
	FOREIGN KEY (user_id) REFERENCES Accounts (user_id)
);

-- Insert data into Books table
INSERT INTO Books (title, author, publisher, total_copies, copies_borrowed, copies_available, [description], IMG, created_date, created_user, updated_date, updated_user, is_deleted)
VALUES
    ('Harry Potter and the Sorcerer''s Stone', 'J.K. Rowling', 'Scholastic', 10, 2, 8, 'A magical adventure.', 'https://i.imgur.com/fXXpcve.jpg', GETDATE(), 'Admin', GETDATE(), 'Admin', 0),
    ('The Great Gatsby', 'F. Scott Fitzgerald', 'Scribner', 5, 1, 4, 'A classic novel.', 'https://i.imgur.com/Kg0Kl8p.jpg', GETDATE(), 'Admin', GETDATE(), 'Admin', 0),
    ('To Kill a Mockingbird', 'Harper Lee', 'HarperCollins', 8, 0, 8, 'A compelling story.', 'https://i.imgur.com/9fw99Qa.jpg', GETDATE(), 'Admin', GETDATE(), 'Admin', 0);

-- Insert data into BookDetail table
INSERT INTO BookDetail (ISBN, book_id, title, author, publisher, publish_date, created_date, status, is_borrowed, updated_date, updated_user, is_deleted)
VALUES
    ('978-0439554930', 1, 'Harry Potter and the Sorcerer''s Stone', 'J.K. Rowling', 'Scholastic', '2001-09-11', GETDATE(), 'Available', 0, GETDATE(), 'Admin', 0),
    ('978-0743273565', 2, 'The Great Gatsby', 'F. Scott Fitzgerald', 'Scribner', '2004-09-30', GETDATE(), 'Available', 0, GETDATE(), 'Admin', 0),
    ('978-0061120084', 3, 'To Kill a Mockingbird', 'Harper Lee', 'HarperCollins', '2006-05-23', GETDATE(), 'Available', 0, GETDATE(), 'Admin', 0);

-- Insert data into Accounts table (Users)
INSERT INTO Accounts (username, password, full_name, email, phone_number, address, role, IMG, created_date, updated_date, updated_user, is_deleted)
VALUES
    ('user1', '123456', 'John Doe', 'john.doe@example.com', '123-456-7890', '123 Main St', 'User', 'user1.jpg', GETDATE(), GETDATE(), 'Admin', 0),
    ('user2', '123456', 'Jane Smith', 'jane.smith@example.com', '987-654-3210', '456 Elm St', 'User', 'user2.jpg', GETDATE(), GETDATE(), 'Admin', 0),
    ('admin', '123456', 'Admin User', 'admin@example.com', '555-555-5555', '789 Oak St', 'Admin', 'admin.jpg', GETDATE(), GETDATE(), 'Admin', 0);

-- Insert data into Genres table
INSERT INTO Genres (genre_name)
VALUES
    ('Fantasy'),
    ('Classics'),
    ('Fiction'),
	('Action'),
	('Novel');

	INSERT INTO Book_Genre (book_id, genre_id)
VALUES
    (1, 1),
    (2, 1),
    (3, 3),
    (1, 4),
	(3, 4),
	(2, 2);

-- Insert data into Loans table
INSERT INTO Loans (user_id, detail_id, borrow_date, due_date, returned_date, created_date, created_user, request_date, expried_date, is_returned, is_approved)
VALUES
    (1, 1, GETDATE(), DATEADD(DAY, 14, GETDATE()), NULL, GETDATE(), 'Admin', NULL, NULL, 0, 1),
    (2, 2, GETDATE(), DATEADD(DAY, 21, GETDATE()), NULL, GETDATE(), 'Admin', NULL, NULL, 0, 1);

-- Insert data into Fine table
INSERT INTO Fine (user_id, loan_id, fine_amount, [description], created_date, created_user, is_paid)
VALUES
    (1, 1, 0, 'No fine', GETDATE(), 'Admin', 1),
    (2, 2, 0, 'No fine', GETDATE(), 'Admin', 1);

-- Insert data into Request table
INSERT INTO Request (user_id, loan_id, request_date, [description], created_date, created_user, is_approved)
VALUES
    (1, 1, GETDATE(), 'Extend due date', GETDATE(), 'Admin', 0),
    (2, 2, GETDATE(), 'Request for book renewal', GETDATE(), 'Admin', 0);

-- Insert data into Comment table
INSERT INTO Comment (user_id, book_id, content, rep_comment, created_date, created_user, updated_date, updated_user, is_deleted)
VALUES
    (1, 1, 'Great book!', 0, GETDATE(), 'User1', NULL, NULL, 0),
    (2, 1, 'I love this series!', 1, GETDATE(), 'User2', NULL, NULL, 0),
    (1, 2, 'Classic literature at its best.', 0, GETDATE(), 'User1', NULL, NULL, 0);

