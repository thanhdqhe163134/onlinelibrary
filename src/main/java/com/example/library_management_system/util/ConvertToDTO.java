package com.example.library_management_system.util;


import com.example.library_management_system.dto.AccountResponse;
import com.example.library_management_system.dto.BookResponse;
import com.example.library_management_system.entity.Account;
import com.example.library_management_system.entity.Books;
import com.example.library_management_system.entity.Genres;

import java.util.stream.Collectors;

public class ConvertToDTO {

    public AccountResponse convertToAccountResponse(Account account) {

        AccountResponse response = new AccountResponse();
        response.setUserId(account.getUserId());
        response.setUsername(account.getUsername());
        response.setFullName(account.getFullName());
        response.setEmail(account.getEmail());
        response.setPhoneNumber(account.getPhoneNumber());
        response.setAddress(account.getAddress());
        response.setImg(account.getImg());
        response.setCreatedDate(account.getCreatedDate());
        response.setUpdatedDate(account.getUpdatedDate());
        response.setUpdatedUser(account.getUpdatedUser());
        response.setRole(account.getRole());
        response.setIsDeleted(account.getIs_deleted());

        return response;
    }

    public Account convertToEntity(AccountResponse response) {
        Account account = new Account();
        account.setUserId(response.getUserId());
        account.setUsername(response.getUsername());
        account.setFullName(response.getFullName());
        account.setEmail(response.getEmail());
        account.setPhoneNumber(response.getPhoneNumber());
        account.setAddress(response.getAddress());
        account.setImg(response.getImg());
        account.setCreatedDate(response.getCreatedDate());
        account.setUpdatedUser(response.getUpdatedUser(account.getUpdatedUser()));
        account.setRole(response.getRole());

        return account;
    }

    public BookResponse convertToBookResponse(Books book) {
        BookResponse bookResponse = new BookResponse();
        bookResponse.setBook_id(book.getBook_id());
        bookResponse.setTitle(book.getTitle());
        bookResponse.setAuthor(book.getAuthor());
        bookResponse.setPublisher(book.getPublisher());
        bookResponse.setTotal_copies(book.getTotal_copies());
        bookResponse.setCopies_borrowed(book.getCopies_borrowed());
        bookResponse.setCopies_available(book.getCopies_available());
        bookResponse.setDescription(book.getDescription());
        bookResponse.setImg(book.getIMG());

        bookResponse.setGenres(book.getGenres().stream().map(Genres::getGenre_name).collect(Collectors.toSet()));
        return bookResponse;
    }
}