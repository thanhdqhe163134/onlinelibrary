package com.example.library_management_system;

import com.example.library_management_system.entity.Account;
import com.example.library_management_system.repository.AccountRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class InitialDataLoader {

    private final AccountRepository accountRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    public InitialDataLoader(AccountRepository accountRepository, BCryptPasswordEncoder passwordEncoder) {
        this.accountRepository = accountRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Bean
    public CommandLineRunner loadData() {
        return args -> {
            Account admin = accountRepository.findByUsername("admin");
            if (admin == null) {
                admin = new Account();
                admin.setUsername("admin");
                admin.setRole("Admin");
            }
            admin.setPassword(passwordEncoder.encode("123456"));
            accountRepository.save(admin);

            Account user = accountRepository.findByUsername("user");
            if (user == null) {
                user = new Account();
                user.setUsername("user");
                user.setRole("User");
            }
            user.setPassword(passwordEncoder.encode("123456"));
            accountRepository.save(user);
        };
    }
}
