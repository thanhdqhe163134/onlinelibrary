package com.example.library_management_system.controller;

import com.example.library_management_system.dto.BookRequest;
import com.example.library_management_system.dto.BookResponse;
import com.example.library_management_system.entity.Books;
import com.example.library_management_system.entity.Genres;
import com.example.library_management_system.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/books")
@CrossOrigin(origins = "http://localhost:3000")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping
    public List<BookResponse> getAllBooks() {
        return bookService.getAllBooks();
    }

    @GetMapping("/{id}")
    public ResponseEntity<BookResponse> getBookById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(bookService.getBookById(id));
    }


    @GetMapping("/search")
    public ResponseEntity<List<BookResponse>> searchBooks(@RequestParam("keyword") String keyword) {
        List<BookResponse> foundBooks = bookService.searchBooksByKeyword(keyword);
        if (!foundBooks.isEmpty()) {
            return new ResponseEntity<>(foundBooks, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping("/searchByGenre")
    public ResponseEntity<List<BookResponse>> searchBooksByGenre(@RequestParam("genre") String genre) {
        List<BookResponse> foundBooks = bookService.searchBooksByGenre(genre);

        if (!foundBooks.isEmpty()) {
            return new ResponseEntity<>(foundBooks, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/genres/{genre}")
    public ResponseEntity<String> deleteGenre(@PathVariable("genre") String genre){
        try {
            bookService.deleteGenre(genre);
            return new ResponseEntity<>("Genre deleted successfully", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Error occurred while deleting genre", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @GetMapping("/genres")
    public ResponseEntity<List<String>> getAllGenres() {
        List<String> allGenres = bookService.getAllGenres();

        if (!allGenres.isEmpty()) {
            return new ResponseEntity<>(allGenres, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }




    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteBook(@PathVariable("id") Long id, Principal principal) {

        boolean isDeleted = bookService.deleteBook(id);
        if (isDeleted) {
            return new ResponseEntity<>("Book deleted successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Failed to delete book", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}/restore")
    public ResponseEntity<String> restoreBook(@PathVariable("id") Long id) {
        // Lấy thông tin người dùng đang đăng nhập
        String loggedInUsername = "admin"; // Hoặc lấy từ xác thực

        // Gọi service để khôi phục cuốn sách và cập nhật thông tin người cập nhật và thời gian cập nhật
        boolean isRestored = bookService.restoreBook(id, loggedInUsername);

        if (isRestored) {
            // Trả về mã trạng thái 200 (OK) và thông báo thành công
            return new ResponseEntity<>("Book restored successfully", HttpStatus.OK);
        } else {
            // Trả về mã trạng thái 500 (Internal Server Error) và thông báo lỗi
            return new ResponseEntity<>("Failed to restore book", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Books> updateBook(@PathVariable Long id, @RequestBody BookRequest bookRequest) {
        try {
            boolean updatedBook = bookService.updateBook(id, bookRequest);
            if (updatedBook) {
                return new ResponseEntity<>( HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping
    public ResponseEntity<String> addBook(@RequestBody BookRequest bookRequest) {

        try {
            Books newBook = bookService.addBook(bookRequest);
            if (newBook != null) {
                return new ResponseEntity<>("Book added successfully", HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>("Failed to add book", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } catch (Exception e) {
            return new ResponseEntity<>("An error occurred: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }




}




