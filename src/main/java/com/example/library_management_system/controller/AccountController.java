package com.example.library_management_system.controller;


import com.example.library_management_system.dto.AccountUpdateRequest;
import com.example.library_management_system.security.JwtUtil;
import com.example.library_management_system.dto.AccountResponse;
import com.example.library_management_system.dto.LoginRequest;
import com.example.library_management_system.entity.Account;
import com.example.library_management_system.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/accounts")
@CrossOrigin(origins = "http://localhost:3000")

public class AccountController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;



    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginRequest loginRequest) {
        String username = loginRequest.getUsername();
        String password = loginRequest.getPassword();
        Account account = accountService.loadUserByUsername(username);
        if (account != null && passwordEncoder.matches(password, account.getPassword())) {
            String jwt = jwtUtil.generateToken(username);
            Map<String, String> response = new HashMap<>();
            response.put("jwt", jwt);
            response.put("username", username);
            response.put("role", account.getRole());
            return ResponseEntity.ok(response);
        }
        throw new RuntimeException("Invalid credentials");
    }

    @GetMapping
    public ResponseEntity<?> getAllAccounts() {
        return ResponseEntity.ok(accountService.getAllAccounts());
    }

    @GetMapping("/{username}")
    public ResponseEntity<AccountResponse> getProfile(@PathVariable String username) {
        return ResponseEntity.ok(accountService.getProfile(username));
    }
    @PutMapping("/{username}")
    public ResponseEntity<AccountResponse> updateProfile(@PathVariable String username, @RequestBody AccountUpdateRequest request) {
        return ResponseEntity.ok(accountService.updateProfile(username, request));
    }







}

