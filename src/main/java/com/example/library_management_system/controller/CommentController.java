package com.example.library_management_system.controller;

import com.example.library_management_system.dto.CommentResponse;
import com.example.library_management_system.entity.Comment;
import com.example.library_management_system.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/comments")
@CrossOrigin(origins = "http://localhost:3000")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @GetMapping("/{book_id}")
    public ResponseEntity<List<CommentResponse>> getCommentsByBookId(@PathVariable("book_id") Long book_id) {
        List<CommentResponse> comments = commentService.getCommentsByBookId(book_id);
        if (!comments.isEmpty()) {
            return new ResponseEntity<>(comments, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @PostMapping
    public ResponseEntity<Comment> addComment(@RequestBody Comment comment) {
        Comment newComment = commentService.saveComment(comment);
        if (newComment != null) {
            return new ResponseEntity<>(HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/{comment_id}")
    public ResponseEntity<Comment> updateComment(@PathVariable("comment_id") Long comment_id, @RequestBody Comment updatedComment) {
        Comment existingComment = commentService.getCommentById(comment_id);
        if (existingComment != null) {
            existingComment.setContent(updatedComment.getContent());
            Comment UpdateComment = commentService.UpdateComment(existingComment);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{comment_id}")
    public ResponseEntity<Void> deleteComment(@PathVariable("comment_id") Long comment_id) {
        Comment existingComment = commentService.getCommentById(comment_id);
        if (existingComment != null) {
            commentService.deleteComment(comment_id);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
