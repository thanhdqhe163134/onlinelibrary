package com.example.library_management_system.dto;

import com.example.library_management_system.entity.Books;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.List;

public class BookRequest {

    private Long book_id;
    private String title;
    private String author;
    private String publisher;
    private int total_copies;

    private int copies_borrowed;

    private  int copies_available;
    private String description;
    private String img;
    private List<String> genres;


    public Books toBook() {
        Books book = new Books();
        book.setBook_id(this.book_id);
        book.setTitle(this.title);
        book.setAuthor(this.author);
        book.setPublisher(this.publisher);
        book.setTotal_copies(this.total_copies);
        book.setCopies_borrowed(this.copies_borrowed);
        book.setCopies_available(this.copies_available);
        book.setDescription(this.description);
        book.setIMG(this.img);
        return book;
    }

    public Long getBook_id() {
        return book_id;
    }

    public void setBook_id(Long book_id) {
        this.book_id = book_id;
    }

    public int getCopies_borrowed() {
        return copies_borrowed;
    }

    public void setCopies_borrowed(int copies_borrowed) {
        this.copies_borrowed = copies_borrowed;
    }

    public int getCopies_available() {
        return copies_available;
    }

    public void setCopies_available(int copies_available) {
        this.copies_available = copies_available;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getTotal_copies() {
        return total_copies;
    }

    public void setTotal_copies(int total_copies) {
        this.total_copies = total_copies;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}

