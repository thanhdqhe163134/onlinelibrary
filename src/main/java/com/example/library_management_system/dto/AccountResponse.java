package com.example.library_management_system.dto;

import java.sql.Date;

public class AccountResponse {
    private long userId;
    private String username;
    private String fullName;
    private String email;
    private String phoneNumber;
    private String address;
    private String img;
    private Date createdDate;

    private Date updatedDate;

    private String updatedUser;
    private String role;

    private Boolean isDeleted;

    public AccountResponse() {
    }

    public AccountResponse(long userId, String username, String fullName, String email, String phoneNumber, String address, String img, Date createdDate, String role) {
        this.userId = userId;
        this.username = username;
        this.fullName = fullName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.img = img;
        this.createdDate = createdDate;
        this.role = role;
    }

    public AccountResponse(long userId, String username, String fullName, String email, String phoneNumber, String address, String img, Date createdDate, Date updatedDate, String updatedUser, String role) {
        this.userId = userId;
        this.username = username;
        this.fullName = fullName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.img = img;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.updatedUser = updatedUser;
        this.role = role;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedUser(String updatedUser) {
        return this.updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
}
