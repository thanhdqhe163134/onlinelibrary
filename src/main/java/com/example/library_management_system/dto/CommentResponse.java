package com.example.library_management_system.dto;

import com.example.library_management_system.entity.Comment;

import java.sql.Date;

public class CommentResponse {
    private long comment_id;
    private long user_id;
    private long book_id;
    private String content;
    private long rep_comment;
    private Date created_date;
    private String created_user;
    private Date updated_date;
    private String updated_user;
    private boolean is_deleted;

    public CommentResponse() {
    }


    public CommentResponse(Comment comment) {
        this.comment_id = comment.getComment_id();
        this.user_id = comment.getUser_id();
        this.book_id = comment.getBook_id();
        this.content = comment.getContent();
        this.rep_comment = comment.getRep_comment();
        this.created_date = comment.getCreated_date();
        this.created_user = comment.getCreated_user();
        this.updated_date = comment.getUpdated_date();
        this.updated_user = comment.getUpdated_user();
        this.is_deleted = comment.getIs_deleted();
    }

    public long getComment_id() {
        return comment_id;
    }

    public void setComment_id(long comment_id) {
        this.comment_id = comment_id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public long getBook_id() {
        return book_id;
    }

    public void setBook_id(long book_id) {
        this.book_id = book_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getRep_comment() {
        return rep_comment;
    }

    public void setRep_comment(long rep_comment) {
        this.rep_comment = rep_comment;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public String getCreated_user() {
        return created_user;
    }

    public void setCreated_user(String created_user) {
        this.created_user = created_user;
    }

    public Date getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(Date updated_date) {
        this.updated_date = updated_date;
    }

    public String getUpdated_user() {
        return updated_user;
    }

    public void setUpdated_user(String updated_user) {
        this.updated_user = updated_user;
    }

    public boolean isIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(boolean is_deleted) {
        this.is_deleted = is_deleted;
    }
}
