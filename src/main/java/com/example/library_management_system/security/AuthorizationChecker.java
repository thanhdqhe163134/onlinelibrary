package com.example.library_management_system.security;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class AuthorizationChecker {

    public void checkPermission(String username) {
        String currentUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        boolean isAdmin = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
                .stream()
                .anyMatch(grantedAuthority -> "Admin".equals(grantedAuthority.getAuthority()));

        if (!isAdmin && !currentUsername.equals(username)) {
            throw new AccessDeniedException("You don't have permission to access this resource");
        }
    }

    public void checkAdmin() {
        boolean isAdmin = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
                .stream()
                .anyMatch(grantedAuthority -> "Admin".equals(grantedAuthority.getAuthority()));

        if (!isAdmin) {
            throw new AccessDeniedException("You don't have permission to access this resource");
        }
    }
}
