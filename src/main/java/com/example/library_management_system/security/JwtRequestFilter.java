package com.example.library_management_system.security;


import com.example.library_management_system.entity.Account;
import com.example.library_management_system.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtRequestFilter extends OncePerRequestFilter {

    @Autowired
    private AccountService accountService;

    @Autowired
    private JwtUtil jwtUtil;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        logger.info("Filtering the request");

        String path = request.getRequestURI();
        String method = request.getMethod();


        if ("GET".equals(method) &&
                ("/api/books".equals(path)
                        || "/api/books/genres".equals(path)
                        || "/api/books/search".equals(path)
                        || "/api/books/searchByGenre".equals(path)
                        || "/api/books/**".equals(path)

                        ) ) {
            chain.doFilter(request, response);
            return;
        }

        // if ("/someSpecificPath".equals(path) && "POST".equals(method)) {
        //    chain.doFilter(request, response);
        //    return;
        //}

        final String authorizationHeader = request.getHeader("Authorization");

        String username = null;
        String jwt = null;

        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            jwt = authorizationHeader.substring(7);
            username = jwtUtil.extractUsername(jwt);
        }

        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            Account account = this.accountService.loadUserByUsername(username);

            if (jwtUtil.validateToken(jwt, account.getUsername())) {
                UserDetails userDetails = User.withUsername(account.getUsername())
                        .password(account.getPassword())
                        .authorities(account.getRole())
                        .build();

                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                        new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());

                usernamePasswordAuthenticationToken
                        .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }

        chain.doFilter(request, response);
    }

}
