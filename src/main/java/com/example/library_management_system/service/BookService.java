package com.example.library_management_system.service;


import com.example.library_management_system.dto.BookRequest;
import com.example.library_management_system.dto.BookResponse;
import com.example.library_management_system.entity.Books;
import com.example.library_management_system.entity.Genres;
import com.example.library_management_system.repository.BookRepository;
import com.example.library_management_system.repository.GenresRepository;
import com.example.library_management_system.util.ConvertToDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private GenresRepository genresRepository;

    ConvertToDTO convertToDTO = new ConvertToDTO();

    private String getCurrentUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || !authentication.isAuthenticated()) {
            return null;
        }
        Object principal = authentication.getPrincipal();
        if (principal instanceof UserDetails) {
            UserDetails userDetails = (UserDetails) principal;
            return userDetails.getUsername();
        } else {
            return principal.toString();
        }
    }

    public List<BookResponse> getAllBooks() {
        // Load data from the database
        List<Books> allBooks = bookRepository.findAll();
        List<Books> nonDeletedBooks = allBooks.stream()
                .filter(book -> !book.getIs_deleted())
                .collect(Collectors.toList());

        // Convert each Books entity to BookResponse DTO
        List<BookResponse> bookResponses = new ArrayList<>();
        for (Books book : nonDeletedBooks) {
            BookResponse response = new BookResponse();
            response = convertToDTO.convertToBookResponse(book);

            // Convert Genres to Set of genre names
            Set<String> genreNames = book.getGenres().stream()
                    .map(Genres::getGenre_name)
                    .collect(Collectors.toSet());
            response.setGenres(genreNames);
            bookResponses.add(response);
        }

        return bookResponses;
    }


    public Books addBook(BookRequest bookRequest) {
        Books book = new Books();
        book.setTitle(bookRequest.getTitle());
        book.setAuthor(bookRequest.getAuthor());
        book.setPublisher(bookRequest.getPublisher());
        book.setTotal_copies(bookRequest.getTotal_copies());
        book.setCopies_borrowed(0);
        book.setCopies_available(0);
        book.setDescription(bookRequest.getDescription());
        book.setIMG(bookRequest.getImg());
        book.setIs_deleted(false);
        book.setCreated_user(getCurrentUsername());
        book.setCreated_date(new Date(System.currentTimeMillis()));
        book.setUpdated_user("Admin");
        book.setUpdated_date(new Date(System.currentTimeMillis()));

        List<String> genreNames = bookRequest.getGenres();
        Set<Genres> genres = new HashSet<>();
        if (genreNames != null && !genreNames.isEmpty()) {
            for (String name : genreNames) {
                Genres genre = genresRepository.findByName(name);
                if (genre == null) {
                    genre = new Genres();
                    genre.setGenre_name(name);
                    genre = genresRepository.save(genre); // Lưu genre trước khi thêm vào set
                }
                genres.add(genre);
            }
            book.setGenres(genres); // Thiết lập genres sau khi tất cả đã được lưu
        }
        return bookRepository.save(book); // Lưu sách kể cả khi không có genres
    }




    public BookResponse getBookById(Long id) {
        Books book = bookRepository.findById(id).orElseThrow(() -> new RuntimeException("Book not found"));
        return convertToDTO.convertToBookResponse(book);
    }

    public List<BookResponse> searchBooksByKeyword(String keyword) {
        List<Books> foundBooks = bookRepository.findByKeyword(keyword);
        List<Books> nonDeletedBooks = foundBooks.stream()
                .filter(book -> !book.getIs_deleted())
                .collect(Collectors.toList());

        List<BookResponse> bookResponses = new ArrayList<>();
        for (Books book : nonDeletedBooks) {
            bookResponses.add(convertToDTO.convertToBookResponse(book));
        }
        return bookResponses;
    }
    public boolean deleteBook(Long id) {
        // Lấy thông tin cuốn sách từ id
        Books book = bookRepository.findById(id).orElse(null);

        // Kiểm tra xem cuốn sách có tồn tại không
        if (book != null) {
            // Thiết lập thông tin người cập nhật và thời gian cập nhật
            book.setUpdated_user(getCurrentUsername());
            book.setUpdated_date(new Date(System.currentTimeMillis()));
            book.setIs_deleted(true);

            // Lưu thông tin cuốn sách đã bị xóa vào cơ sở dữ liệu
            bookRepository.save(book);
            return true;
        } else {
            return false;
        }
    }

    public boolean restoreBook(Long id, String loggedInUsername) {
        // Lấy thông tin cuốn sách từ id
        Books book = bookRepository.findById(id).orElse(null);

        // Kiểm tra xem cuốn sách có tồn tại và đã bị xóa không
        if (book != null && book.getIs_deleted()) {
            // Thiết lập lại trạng thái is_deleted và thông tin người cập nhật và thời gian cập nhật
            book.setIs_deleted(false);
            book.setUpdated_user(loggedInUsername);
            book.setUpdated_date(new Date(System.currentTimeMillis()));

            // Lưu thông tin cuốn sách đã được khôi phục vào cơ sở dữ liệu
            bookRepository.save(book);
            return true;
        } else {
            return false;
        }
    }


    public List<BookResponse> searchBooksByGenre(String genre) {
        List<Books> allBooks = bookRepository.findAll();

        // Filter books based on the genre and whether they are deleted or not
        List<Books> filteredBooks = allBooks.stream()
                .filter(book -> !book.getIs_deleted())
                .filter(book -> book.getGenres().stream().anyMatch(g -> g.getGenre_name().equalsIgnoreCase(genre)))
                .collect(Collectors.toList());
        List<BookResponse> bookResponses = new ArrayList<>();
        for (Books book : filteredBooks) {
            BookResponse response = new BookResponse();
            response = convertToDTO.convertToBookResponse(book);

            // Convert Genres to Set of genre names
            Set<String> genreNames = book.getGenres().stream()
                    .map(Genres::getGenre_name)
                    .collect(Collectors.toSet());
            response.setGenres(genreNames);
            bookResponses.add(response);
        }

        return bookResponses;


    }

    public List<String> getAllGenres() {
        List<Genres> genres = genresRepository.findAll();
        return genres.stream().map(Genres::getGenre_name).collect(Collectors.toList());
    }

    public void deleteGenre(String  genre) throws Exception {
        Genres genres = genresRepository.findByName(genre);
        if (genres != null) {
            genresRepository.delete(genres);
        } else {
            throw new Exception("Genre not found");
        }



    }



    public Boolean updateBook(Long id, BookRequest bookRequest) {
        Optional<Books> bookOptional = bookRepository.findById(id);
        if (bookOptional.isPresent()) {
            Books book = bookOptional.get();
            book.setTitle(bookRequest.getTitle());
            book.setAuthor(bookRequest.getAuthor());
            book.setPublisher(bookRequest.getPublisher());
            book.setTotal_copies(bookRequest.getTotal_copies());
            book.setDescription(bookRequest.getDescription());
            book.setIMG(bookRequest.getImg());
            book.setUpdated_user(getCurrentUsername());
            book.setUpdated_date(new Date(System.currentTimeMillis()));
            List<String> genreNames = bookRequest.getGenres();
            Set<Genres> genres = new HashSet<>();
            if (genreNames != null && !genreNames.isEmpty()) {
                for (String name : genreNames) {
                    Genres genre = genresRepository.findByName(name);
                    if (genre == null) {
                        genre = new Genres();
                        genre.setGenre_name(name);
                        genresRepository.save(genre);
                    }
                    genres.add(genre);
                }
            }
            book.setGenres(genres);
            bookRepository.save(book);
            return true;
        } else {
            return false;
        }
    }
}
