package com.example.library_management_system.service;

import com.example.library_management_system.dto.AccountResponse;
import com.example.library_management_system.dto.AccountUpdateRequest;
import com.example.library_management_system.entity.Account;
import com.example.library_management_system.repository.AccountRepository;

import com.example.library_management_system.security.AuthorizationChecker;
import com.example.library_management_system.util.ConvertToDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    ConvertToDTO convertToDTO = new ConvertToDTO();

    AuthorizationChecker authorizationChecker = new AuthorizationChecker();

    public List<AccountResponse> getAllAccounts() {
        List<Account> accounts = accountRepository.findAll();
        return accounts.stream().map(account -> convertToDTO.convertToAccountResponse(account)).collect(Collectors.toList());
    }


    public Account loadUserByUsername(String username) {
        return accountRepository.findByUsername(username);
    }


    public AccountResponse getProfile(String username) {
        authorizationChecker.checkPermission(username); // Kiểm tra quyền

        Account account = accountRepository.findByUsername(username);
        return convertToDTO.convertToAccountResponse(account);
    }

    public AccountResponse updateProfile(String username, AccountUpdateRequest request) {
        authorizationChecker.checkPermission(username);
        Account account = accountRepository.findByUsername(username);
        if (account != null) {
            // Update account entity from request DTO
            account.setFullName(request.getFullName());
            account.setEmail(request.getEmail());
            account.setPhoneNumber(request.getPhoneNumber());
            account.setAddress(request.getAddress());
            account.setImg(request.getImg());
            account.setUpdatedUser(username);
            account.setUpdatedDate(new Date(System.currentTimeMillis()));
            account.setRole(request.getRole());
            // Save the updated account entity
            Account updatedAccount = accountRepository.save(account);

            // Convert the updated account entity to DTO and return
            return convertToDTO.convertToAccountResponse(updatedAccount);
        } else {
            // Handle user not found case
            throw new RuntimeException("User not found with username: " + username);
        }
    }

}
