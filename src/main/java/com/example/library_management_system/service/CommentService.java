package com.example.library_management_system.service;

import com.example.library_management_system.dto.CommentResponse;
import com.example.library_management_system.entity.Comment;
import com.example.library_management_system.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;

    public List<CommentResponse> getCommentsByBookId(Long book_id) {
        List<Comment> comments = commentRepository.findByBookId(book_id);

        // lọc ra những bình luận có is_deleted = true nếu là true thì set content = "This comment has been deleted"
        List<CommentResponse> commentResponses = comments.stream().map(comment -> {
            if (comment.getIs_deleted()) {
                comment.setContent("This comment has been deleted");
            }
            return new CommentResponse(comment);
        }).collect(Collectors.toList());

        return commentResponses;
    }



    public Comment saveComment(Comment comment) {
        comment.setCreated_user("admin");
        comment.setCreated_date(new Date(System.currentTimeMillis()));
        return commentRepository.save(comment);
    }


    public Comment getCommentById(Long comment_id) {
        Comment comment = commentRepository.findById(comment_id).orElse(null);
        if (comment != null && !comment.getIs_deleted()) {
            return comment;
        } else {
            return null;
        }
    }

    public Comment UpdateComment(Comment existingComment) {
        existingComment.setUpdated_date(new Date(System.currentTimeMillis()));
        existingComment.setUpdated_user("admin");
        return commentRepository.save(existingComment);
    }

    public void deleteComment(Long comment_id) {
        Comment comment = commentRepository.findById(comment_id).orElse(null);
        comment.setUpdated_date(new Date(System.currentTimeMillis()));
        comment.setUpdated_user("admin");
            if (comment != null) {
            comment.setIs_deleted(true);
            commentRepository.save(comment);
        }


    }
}
