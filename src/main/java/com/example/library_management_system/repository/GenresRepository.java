package com.example.library_management_system.repository;

import com.example.library_management_system.entity.Genres;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GenresRepository extends JpaRepository<Genres, Long> {

    @Query("SELECT g FROM Genres g WHERE g.genre_name = :name")
    Genres findByName(@Param("name") String name);
}

