package com.example.library_management_system.repository;

import com.example.library_management_system.entity.Books;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BookRepository extends JpaRepository<Books, Long> {
    @Query("SELECT b FROM Books b WHERE LOWER(b.title) LIKE LOWER(concat('%', :keyword, '%')) OR " +
            "LOWER(b.author) LIKE LOWER(concat('%', :keyword, '%')) OR " +
            "LOWER(b.publisher) LIKE LOWER(concat('%', :keyword, '%'))")
    List<Books> findByKeyword(@Param("keyword") String keyword);


}
